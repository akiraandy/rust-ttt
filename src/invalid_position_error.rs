use std::error::Error;
use std::fmt;
use board::Board;

#[derive(Debug, PartialEq)]
pub struct InvalidPositionError {
    pub details: String
}

impl InvalidPositionError {
    pub fn new(board: &Board) -> InvalidPositionError {
        let mut formatted_vec = Vec::new();
        board.clone().available_spaces().into_iter().for_each(|space| formatted_vec.push(space + 1));
        InvalidPositionError{ details: format!("Invalid board position. Available positions include: {:?}", formatted_vec)}
    }
}

impl fmt::Display for InvalidPositionError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.details)
    }
}

impl Error for InvalidPositionError {
    fn description(&self) -> &str {
        &self.details
    }
}
