use std::error::Error;
use std::fmt;

#[derive(Debug, PartialEq)]
pub struct InvalidTerminalStateError {
    pub details: String
}

impl InvalidTerminalStateError {
    pub fn new(msg: &str) -> InvalidTerminalStateError  {
        InvalidTerminalStateError  { details: msg.to_string() }
    }
}

impl fmt::Display for InvalidTerminalStateError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.details)
    }
}

impl Error for InvalidTerminalStateError  {
    fn description(&self) -> &str {
        &self.details
    }
}
