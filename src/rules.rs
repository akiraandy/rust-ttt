use board::Board;
use marker::Marker;
use invalid_terminal_state_error::InvalidTerminalStateError;
use game_state::GameState;

pub fn game_over(board: &Board) -> bool {
    match get_game_state(board) {
        GameState::Won => true,
        GameState::Tie => true,
        GameState::NotOver => false,
        GameState::Error => true
    }
}

pub fn get_game_state(board: &Board) -> GameState {
    match is_winner(board) {
        Ok(result) => match result {
            true => GameState::Won,
            false => {
                if board.full() {
                    GameState::Tie
                } else {
                    GameState::NotOver
                }
            }
        },
        Err(_) => GameState::Error
    }
}

pub fn is_winner(board: &Board) -> Result<bool, InvalidTerminalStateError> {
    let combos = get_all_win_combos(board);
    if combos.contains(&vec![Marker::X; board.row_size]) && combos.contains(&vec![Marker::O; board.row_size]) {
        Err(InvalidTerminalStateError::new("More than one winner on board."))
    } else {
       Ok(combos.into_iter().any( |combo| combo == vec![Marker::X; board.row_size] || combo == vec![Marker::O; board.row_size]))
    }
}

pub fn get_winner(board: &Board) -> Option<Marker> {
    match is_winner(board) {
        Ok(_boolean) => {
            let combos = get_all_win_combos(board);
            let some_winner = combos.into_iter().find( |combo| combo == &vec![Marker::X; board.row_size] || combo == &vec![Marker::O; board.row_size]);
            match some_winner {
                Some(winner_vec) => Some(winner_vec[0]),
                None => None
            }
        },
        Err(_) => None
    }
}

fn get_all_win_combos(board: &Board) -> Vec<Vec<Marker>> {
    let mut combos :Vec<Vec<Marker>> = Vec::new();
    let mut rows = get_rows(board);
    let mut cols = get_cols(board);
    let mut diagonals = get_diagonals(board);

    while !rows.is_empty() { combos.push(rows.pop().unwrap()); }
    while !cols.is_empty() { combos.push(cols.pop().unwrap()); }
    while !diagonals.is_empty() { combos.push(diagonals.pop().unwrap()); }
    combos
}

fn get_rows(board: &Board) -> Vec<Vec<Marker>> {
    let mut result = Vec::new();
    board.spaces.chunks(board.row_size).for_each( |row| result.push(row.to_vec()));
    result
}

fn get_cols(board: &Board) -> Vec<Vec<Marker>> {
    let mut i = 0;
    let mut result = Vec::new();
    while i < board.row_size {
        let mut col = Vec::new();
        let mut j = 0;
        while j < board.spaces.len() {
            let marker = board.spaces[j + i].clone();
            col.push(marker);
            j += board.row_size;
        }
        result.push(col);
        i += 1;
    }
    result
}

fn get_diagonals(board: &Board) -> Vec<Vec<Marker>> {
    let mut i = 0;
    let mut result = Vec::new();
    // get major diagonal
    let mut major_diag = Vec::new();
    while i < board.spaces.len() {
        major_diag.push(board.spaces[i].clone());
        i += board.row_size + 1;
    }
    // get minor diagonal
    let mut minor_diag = Vec::new();
    i = board.row_size - 1;
    while i < board.spaces.len() - 1 {
        minor_diag.push(board.spaces[i].clone());
        i += board.row_size - 1;
    }
    result.push(major_diag);
    result.push(minor_diag);
    result
}

#[cfg(test)]
mod tests {
    use rules;
    use board::Board;
    use marker::Marker;
    use invalid_terminal_state_error::InvalidTerminalStateError;
    use game_state::GameState;

    static ROW_SIZE: usize = 3;

    #[test]
    fn returns_false_if_there_is_no_winner() {
        let mut board: Board = Board::new(ROW_SIZE);
        board.spaces = vec![
            Marker::X, Marker::Empty, Marker::O,
            Marker::Empty, Marker::O, Marker::O,
            Marker::Empty, Marker::Empty, Marker::X
        ];
        assert!(!rules::is_winner(&board).unwrap());
    }

    #[test]
    fn returns_true_if_winner_in_top_row() {
        let mut board: Board = Board::new(ROW_SIZE);
        board.spaces = vec![
                            Marker::X, Marker::X, Marker::X,
                            Marker::Empty, Marker::Empty, Marker::Empty,
                            Marker::Empty, Marker::Empty, Marker::Empty
                            ];
        assert!(rules::is_winner(&board).unwrap());

    }

    #[test]
    fn returns_true_if_winner_in_middle_row() {
        let mut board: Board = Board::new(ROW_SIZE);
        board.spaces = vec![
            Marker::Empty, Marker::Empty, Marker::Empty,
            Marker::O, Marker::O, Marker::O,
            Marker::Empty, Marker::Empty, Marker::Empty
        ];
        assert!(rules::is_winner(&board).unwrap());
    }

    #[test]
    fn returns_true_if_winner_in_bottom_row() {
        let mut board: Board = Board::new(ROW_SIZE);
        board.spaces = vec![
            Marker::Empty, Marker::Empty, Marker::Empty,
            Marker::Empty, Marker::Empty, Marker::Empty,
            Marker::O, Marker::O, Marker::O
        ];
        assert!(rules::is_winner(&board).unwrap());
    }

    #[test]
    fn returns_true_if_winner_in_first_col() {
        let mut board: Board = Board::new(ROW_SIZE);
        board.spaces = vec![
            Marker::O, Marker::Empty, Marker::Empty,
            Marker::O, Marker::Empty, Marker::Empty,
            Marker::O, Marker::Empty, Marker::Empty
        ];
        assert!(rules::is_winner(&board).unwrap());
    }

    #[test]
    fn returns_true_if_winner_in_second_col() {
        let mut board: Board = Board::new(ROW_SIZE);
        board.spaces = vec![
            Marker::Empty, Marker::O, Marker::Empty,
            Marker::Empty, Marker::O, Marker::Empty,
            Marker::Empty, Marker::O, Marker::Empty
        ];
        assert!(rules::is_winner(&board).unwrap());
    }

    #[test]
    fn returns_true_if_winner_in_third_col() {
        let mut board: Board = Board::new(ROW_SIZE);
        board.spaces = vec![
            Marker::Empty, Marker::Empty, Marker::O,
            Marker::Empty, Marker::Empty, Marker::O,
            Marker::Empty, Marker::Empty, Marker::O
        ];
        assert!(rules::is_winner(&board).unwrap());
    }

    #[test]
    fn returns_error_if_there_is_more_than_one_winner_on_the_board() {
        let mut board: Board = Board::new(ROW_SIZE);
        board.spaces = vec![
            Marker::O, Marker::X, Marker::O,
            Marker::O, Marker::X, Marker::O,
            Marker::O, Marker::X, Marker::X
        ];
        assert_eq!(rules::is_winner(&board).unwrap_err(), InvalidTerminalStateError::new("More than one winner on board."));
    }


    #[test]
    fn returns_a_vector_of_cols_as_vectors() {
        let mut board: Board = Board::new(ROW_SIZE);
        board.spaces = vec![
            Marker::X, Marker::Empty, Marker::O,
            Marker::X, Marker::Empty, Marker::O,
            Marker::X, Marker::Empty, Marker::O
        ];
        assert_eq!(rules::get_cols(&board), vec![
                                                vec![Marker::X, Marker::X, Marker::X,],
                                                vec![Marker::Empty, Marker::Empty, Marker::Empty],
                                                vec![Marker::O, Marker::O, Marker::O]]);
    }

    #[test]
    fn returns_a_vector_of_diagonals_as_vectors() {
        let mut board: Board = Board::new(ROW_SIZE);
        board.spaces = vec![
            Marker::X, Marker::Empty, Marker::O,
            Marker::Empty, Marker::X, Marker::Empty,
            Marker::O, Marker::Empty, Marker::X
        ];
        assert_eq!(rules::get_diagonals(&board), vec![
                                                    vec![Marker::X, Marker::X, Marker::X],
                                                    vec![Marker::O, Marker::X, Marker::O]]);
    }

    #[test]
    fn returns_true_if_there_is_a_winner_on_major_diagonal() {
       let mut board: Board = Board::new(ROW_SIZE);
        board.spaces = vec![
            Marker::X, Marker::Empty, Marker::O,
            Marker::Empty, Marker::X, Marker::O,
            Marker::O, Marker::Empty, Marker::X
        ];
        assert!(rules::is_winner(&board).unwrap());
    }

    #[test]
    fn returns_true_if_there_is_a_winner_on_minor_diagonal() {
        let mut board: Board = Board::new(ROW_SIZE);
        board.spaces = vec![
            Marker::Empty, Marker::Empty, Marker::O,
            Marker::Empty, Marker::O, Marker::O,
            Marker::O, Marker::Empty, Marker::X
        ];
        assert!(rules::is_winner(&board).unwrap());
    }

    #[test]
    fn returns_o_marker_if_o_wins() {
        let mut board: Board = Board::new(ROW_SIZE);
        board.spaces = vec![
            Marker::Empty, Marker::Empty, Marker::O,
            Marker::Empty, Marker::O, Marker::X,
            Marker::O, Marker::Empty, Marker::X
        ];
        assert_eq!(rules::get_winner(&board).unwrap(), Marker::O);
    }

    #[test]
    fn returns_x_marker_if_x_wins() {
        let mut board: Board = Board::new(ROW_SIZE);
        board.spaces = vec![
            Marker::Empty, Marker::X, Marker::O,
            Marker::Empty, Marker::X, Marker::O,
            Marker::O, Marker::X, Marker::X
        ];
        assert_eq!(rules::get_winner(&board).unwrap(), Marker::X);
    }

    #[test]
    fn returns_none_if_there_is_more_than_one_winner_on_the_board() {
        let mut board: Board = Board::new(ROW_SIZE);
        board.spaces = vec![
            Marker::O, Marker::X, Marker::O,
            Marker::O, Marker::X, Marker::O,
            Marker::O, Marker::X, Marker::X
        ];
        assert_eq!(rules::get_winner(&board), None);
    }

    #[test]
    fn does_not_return_error_even_if_one_player_wins_more_than_once_on_the_board() {
        let mut board: Board = Board::new(ROW_SIZE);
        board.spaces = vec![
            Marker::O, Marker::X, Marker::O,
            Marker::O, Marker::O, Marker::X,
            Marker::O, Marker::Empty, Marker::X
        ];
            rules::get_winner(&board);
        assert_eq!(rules::get_winner(&board).unwrap(), Marker::O);
    }

    #[test]
    fn returns_none_if_there_is_no_winner() {
        let mut board: Board = Board::new(ROW_SIZE);
        board.spaces = vec![
            Marker::Empty, Marker::Empty, Marker::Empty,
            Marker::Empty, Marker::Empty, Marker::Empty,
            Marker::Empty, Marker::Empty, Marker::Empty
        ];
        assert_eq!(rules::get_winner(&board), None);
    }

    #[test]
    fn returns_true_if_game_is_over() {
        let mut board: Board = Board::new(ROW_SIZE);
        board.spaces = vec![
            Marker::O, Marker::X, Marker::O,
            Marker::O, Marker::O, Marker::X,
            Marker::O, Marker::Empty, Marker::X
        ];
        assert!(rules::game_over(&board));
    }

    #[test]
    fn returns_false_if_game_is_not_over() {
        let mut board: Board = Board::new(ROW_SIZE);
        board.spaces = vec![
            Marker::Empty, Marker::Empty, Marker::Empty,
            Marker::Empty, Marker::Empty, Marker::Empty,
            Marker::Empty, Marker::Empty, Marker::Empty
        ];
        assert!(!rules::game_over(&board));
    }

    #[test]
    fn returns_true_if_game_is_in_error_state() {
        let mut board: Board = Board::new(ROW_SIZE);
        board.spaces = vec![
            Marker::O, Marker::X, Marker::O,
            Marker::O, Marker::X, Marker::O,
            Marker::O, Marker::X, Marker::X
        ];
        assert!(rules::game_over(&board));
    }
    #[test]
    fn returns_won_state_if_game_is_won() {
        let mut board: Board = Board::new(ROW_SIZE);
        board.spaces = vec![
            Marker::Empty, Marker::Empty, Marker::O,
            Marker::Empty, Marker::O, Marker::O,
            Marker::O, Marker::Empty, Marker::X
        ];
        assert_eq!(rules::get_game_state(&board), GameState::Won);
    }

    #[test]
    fn returns_won_state_if_game_is_won1() {
        let mut board: Board = Board::new(ROW_SIZE);
        board.spaces = vec![
            Marker::X, Marker::Empty, Marker::O,
            Marker::Empty, Marker::X, Marker::O,
            Marker::Empty, Marker::Empty, Marker::X
        ];
        assert_eq!(rules::get_game_state(&board), GameState::Won);
    }

    #[test]
    fn returns_tie_state_if_game_is_tied() {
        let mut board =  Board::new(ROW_SIZE);
        board.spaces = vec![
            Marker::X, Marker::O, Marker::X,
            Marker::O, Marker::O, Marker::X,
            Marker::O, Marker::X, Marker::O
        ];
        assert_eq!(rules::get_game_state(&board), GameState::Tie);
    }

    #[test]
    fn returns_not_over_state_if_game_is_not_over() {
        let mut board =  Board::new(ROW_SIZE);
        board.spaces = vec![
            Marker::Empty, Marker::O, Marker::X,
            Marker::O, Marker::O, Marker::X,
            Marker::O, Marker::X, Marker::O
        ];
        assert_eq!(rules::get_game_state(&board), GameState::NotOver);
    }

    #[test]
    fn returns_error_state_if_there_is_an_error_on_the_board() {
        let mut board: Board = Board::new(ROW_SIZE);
        board.spaces = vec![
            Marker::O, Marker::X, Marker::O,
            Marker::O, Marker::X, Marker::O,
            Marker::O, Marker::X, Marker::X
        ];
        assert_eq!(rules::get_game_state(&board), GameState::Error);
    }
}
