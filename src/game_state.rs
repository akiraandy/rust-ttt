#[derive(Debug, PartialEq)]
pub enum GameState {
    Won,
    Tie,
    NotOver,
    Error
}

