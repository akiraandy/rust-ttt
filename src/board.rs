use invalid_position_error::InvalidPositionError;
use marker::Marker;

#[derive(Debug, Clone)]
pub struct Board {
    pub spaces: Vec<Marker>,
    pub row_size: usize,
}


impl Board {

    pub fn new(row_size: usize) -> Board {
        Board {
            spaces: vec![Marker::Empty; row_size * row_size],
            row_size
        }
    }

    fn valid_position(&self, position: i32) -> Result<bool, InvalidPositionError> {
        if position < 0 || position >= (self.spaces.len() as i32) {
           Err(InvalidPositionError::new(self))
        } else {
           Ok(self.spaces[position as usize] == Marker::Empty)
        }
    }

    pub fn place_marker(&self, marker: Marker, position: i32) -> Result<Board, InvalidPositionError> {
        match self.valid_position(position) {
            Ok(bool_result) => match bool_result {
                true => {
                    let mut new_board = self.clone();
                    new_board.spaces[position as usize] = marker;
                    Ok(new_board)
                },
                false => Err(InvalidPositionError::new(self))
            },
            Err(e) => Err(e)
        }
    }

    pub fn full(&self) -> bool {
        self.spaces.iter().all(|space| space == &Marker::X || space == &Marker::O)
    }

    pub fn undo(&mut self, position: usize) {
        self.spaces[position] = Marker::Empty
    }

    pub fn empty(&self) -> bool {
        self.spaces.iter().all(|marker| marker == &Marker::Empty)
    }

    pub fn available_spaces(self) -> Vec<usize> {
        let mut list = Vec::new();
        for (index, marker) in self.spaces.into_iter().enumerate() {
            if marker == Marker::Empty {
                list.push(index);
            }
        }
        list
    }
}

#[cfg(test)]
mod tests {
    use board::Board;
    use invalid_position_error::InvalidPositionError;
    use marker::Marker;

    static ROW_SIZE: usize = 3;

    #[test]
    fn board_can_be_initialized_with_row_size() {
        let board_size: usize = ROW_SIZE * ROW_SIZE;
        let board = Board::new(ROW_SIZE);
        let expected_board_size = board.spaces.len();
        assert_eq!(expected_board_size, board_size)
    }

     #[test]
    fn board_is_initialized_with_empty_enums() {
        let board = Board::new(ROW_SIZE);
        for space in board.spaces {
            assert_eq!(space, Marker::Empty);
        }
    }

    #[test]
    fn can_place_marker_on_board() {
        let board = Board::new(ROW_SIZE);
        assert_eq!(board.place_marker(Marker::X, 0).unwrap().spaces[0], Marker::X);
        assert_eq!(board.place_marker(Marker::O, 1).unwrap().spaces[1], Marker::O);
    }


    #[test]
    fn returns_list_of_available_spaces() {
        let mut board: Board = Board::new(ROW_SIZE);
        board.spaces = vec![
            Marker::Empty, Marker::Empty, Marker::Empty,
            Marker::O, Marker::O, Marker::O,
            Marker::Empty, Marker::Empty, Marker::Empty
        ];
        assert_eq!(board.available_spaces(), vec![0, 1, 2, 6, 7, 8])
    }

    #[test]
    fn returns_true_if_given_a_valid_board_position() {
        let board = Board::new(ROW_SIZE);
        let position = 0;
        let result = board.valid_position(position);
        assert!(result.is_ok());
        let boolean_result = result.unwrap();
        assert!(boolean_result);
    }

    #[test]
    fn returns_invalid_position_error_if_space_is_already_filled() {
        let mut board = Board::new(ROW_SIZE);
        let position: i32  = 0;
        board.spaces[position as usize] = Marker::O;
        let result = board.place_marker(Marker::X, position);
        assert!(result.is_err());
        assert_eq!(result.unwrap_err(), InvalidPositionError { details: String::from("Invalid board position. Available positions include: [2, 3, 4, 5, 6, 7, 8, 9]") });
    }

    #[test]
    fn returns_invalid_position_error_if_position_is_not_valid() {
        let board = Board::new(ROW_SIZE);
        let position = -1;
        let result = board.valid_position(position);
        assert!(result.is_err());
        assert_eq!(result.unwrap_err(), InvalidPositionError { details: String::from( "Invalid board position. Available positions include: [1, 2, 3, 4, 5, 6, 7, 8, 9]") });
    }

    #[test]
    fn returns_true_if_board_is_full() {
        let mut board =  Board::new(ROW_SIZE);
        board.spaces = vec![
            Marker::X, Marker::O, Marker::X,
            Marker::O, Marker::O, Marker::X,
            Marker::O, Marker::X, Marker::O
        ];
        assert!(board.full());
    }

    #[test]
    fn returns_false_if_board_is_not_full() {
        let mut board =  Board::new(ROW_SIZE);
        board.spaces = vec![
            Marker::Empty, Marker::O, Marker::X,
            Marker::O, Marker::O, Marker::X,
            Marker::O, Marker::X, Marker::O
        ];
        assert!(!board.full());
    }

    #[test]
    fn undoes_a_move() {
        let mut board = Board::new(ROW_SIZE);
        board.spaces = vec![
            Marker::Empty, Marker::O, Marker::X,
            Marker::O, Marker::O, Marker::X,
            Marker::O, Marker::X, Marker::O
        ];
        board.undo(1);
        assert_eq!(board.spaces[1], Marker::Empty);
    }

    #[test]
    fn returns_true_if_board_is_empty() {
        let board = Board::new(ROW_SIZE);
        assert!(board.empty());
    }
    #[test]
    fn returns_false_if_board_is_not_empty(){
        let mut board = Board::new(ROW_SIZE);
        board.spaces = vec![
            Marker::Empty, Marker::O, Marker::X,
            Marker::O, Marker::O, Marker::X,
            Marker::O, Marker::X, Marker::O
        ];
        assert!(!board.empty());
    }
}
