#[derive(Copy, Clone, Debug, PartialEq)]
pub enum Marker {
    X, O, Empty
}

impl Marker {
    pub fn to_string(&self) -> String {
        match *self {
            Marker::X => "X".to_string(),
            Marker::O => "O".to_string(),
            Marker::Empty => " ".to_string(),
        }
    }
}

#[cfg(test)]
mod tests {
    use marker::Marker;

    #[test]
    fn returns_string_representation_of_itself() {
        assert_eq!("X", Marker::X.to_string());
        assert_eq!("O", Marker::O.to_string());
        assert_eq!(" ", Marker::Empty.to_string());
    }
}
