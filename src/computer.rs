use board::Board;
use evaluator::Evaluator;
use game_state::GameState;
use marker::Marker;
use messages;
use player::Player;
use rand::thread_rng;
use rand::Rng;
use rules;
use std::cmp::max;
use std::collections::HashMap;

const MAX_DEPTH: i32 = 6;

#[derive(Debug, Clone)]
pub struct Computer {
    pub marker: Marker,
}

impl Computer {
    pub fn new(marker: Marker) -> Computer {
        Computer { marker }
    }

    fn get_next_player(&self, last_move_marker: Marker) -> Marker {
        let opponent_marker = match self.marker {
            Marker::X => Marker::O,
            Marker::O => Marker::X,
            Marker::Empty => Marker::Empty,
        };
        if last_move_marker == opponent_marker {
            self.marker
        } else {
            opponent_marker
        }
    }

    fn get_available_corners(&self, board: &Board) -> Vec<usize> {
        let mut corners = Vec::new();
        corners.push(0);
        corners.push(board.row_size - 1);
        corners.push((board.row_size * board.row_size) - (board.row_size));
        corners.push((board.row_size * board.row_size) - 1);
        corners
            .into_iter()
            .filter(|corner| board.spaces[*corner] == Marker::Empty)
            .collect()
    }

    fn negamax<'a>(
        &self,
        board: Board,
        current_marker: Marker,
        depth: i32,
        mut alpha: i32,
        beta: i32,
        color: i32,
        best_moves: &'a mut HashMap<usize, i32>,
    ) -> (i32, &'a mut HashMap<usize, i32>) {
        let game_state = rules::get_game_state(&board);
        if depth > MAX_DEPTH || game_state == GameState::Won || game_state == GameState::Tie {
            return (
                color * Evaluator::score(&board, self.marker, depth).unwrap(),
                best_moves,
            );
        }
        let mut maximum = -1000;
        let moves = board.clone().available_spaces();
        for m in moves {
            let new_board = board.place_marker(current_marker, m as i32).unwrap();
            let next_player = self.get_next_player(current_marker);
            let negamax_value = -self
                .negamax(
                    new_board,
                    next_player,
                    depth + 1,
                    -beta,
                    -alpha,
                    -color,
                    best_moves,
                )
                .0;
            maximum = max(maximum, negamax_value);
            if depth == 0 {
                best_moves.insert(m, maximum);
            }
            alpha = max(alpha, negamax_value);
            if alpha >= beta {
                break;
            }
        }
        (maximum, best_moves)
    }

    fn choose_move(&self, board: Board) -> i32 {
        let my_marker = self.marker;
        if board.empty() {
            let random_corners = self.get_available_corners(&board);
            random_corners[thread_rng().gen_range(0, random_corners.len())] as i32
        } else {
            let mut moves: HashMap<usize, i32> = HashMap::new();
            let result = self.negamax(board, my_marker, 0, -1000, 1000, 1, &mut moves);
            self.get_best_move(result.1.clone())
        }
    }

    fn get_best_move(&self, moves: HashMap<usize, i32>) -> i32 {
        let mut vec_of_positions_and_scores = Vec::new();
        moves
            .into_iter()
            .for_each(|t| vec_of_positions_and_scores.push(t));
        // sort by position
        vec_of_positions_and_scores.sort_by_key(|l| l.0);
        let copy_of_vec_of_positions_and_scores = vec_of_positions_and_scores.clone();
        let tuple_with_highest_score = vec_of_positions_and_scores.into_iter().max_by_key(|l| l.0);
        let highest_score = tuple_with_highest_score.unwrap().1;
        let (high, _low): (Vec<(usize, i32)>, Vec<(usize, i32)>) =
            copy_of_vec_of_positions_and_scores
                .into_iter()
                .partition(|pos_score| pos_score.1 >= highest_score);
        high[0].0 as i32
    }
}

impl Player for Computer {
    fn take_turn(&self, board: &Board) -> Board {
        println!("{}", messages::computer_thinking(&self.marker));
        let position = self.choose_move(board.clone());
        println!("{}", messages::computer_move(&self.marker, position + 1));
        board.place_marker(self.marker, position).unwrap()
    }
}

#[cfg(test)]
mod tests {

    use board::Board;
    use computer::Computer;
    use marker::Marker;

    const ROW_SIZE: usize = 3;

    #[test]
    fn returns_next_player() {
        let computer = Computer::new(Marker::X);
        assert_eq!(computer.get_next_player(Marker::X), Marker::O);
        assert_eq!(computer.get_next_player(Marker::O), Marker::X);
    }

    #[test]
    fn negamax_returns_optimal_move() {
        let mut board: Board = Board::new(ROW_SIZE);
        let computer = Computer::new(Marker::O);
        board.spaces = vec![
            Marker::O,
            Marker::O,
            Marker::Empty,
            Marker::X,
            Marker::X,
            Marker::Empty,
            Marker::Empty,
            Marker::Empty,
            Marker::Empty,
        ];
        let result = computer.choose_move(board);
        let expected = 2;
        assert_eq!(result, expected);
    }

    #[test]
    fn computer_returns_optimal_move1() {
        let mut board: Board = Board::new(ROW_SIZE);
        let computer = Computer::new(Marker::O);
        board.spaces = vec![
            Marker::Empty,
            Marker::Empty,
            Marker::Empty,
            Marker::O,
            Marker::O,
            Marker::Empty,
            Marker::X,
            Marker::X,
            Marker::Empty,
        ];
        let result = computer.choose_move(board);
        let expected = 5;
        assert_eq!(result, expected);
    }

    #[test]
    fn computer_returns_optimal_move2() {
        let mut board: Board = Board::new(ROW_SIZE);
        let computer = Computer::new(Marker::O);
        board.spaces = vec![
            Marker::X,
            Marker::O,
            Marker::X,
            Marker::O,
            Marker::O,
            Marker::X,
            Marker::X,
            Marker::Empty,
            Marker::Empty,
        ];
        let result = computer.choose_move(board);
        let expected = 7;
        assert_eq!(result, expected);
    }

    #[test]
    fn computer_returns_optimal_move3() {
        let mut board: Board = Board::new(ROW_SIZE);
        let computer = Computer::new(Marker::O);
        board.spaces = vec![
            Marker::O,
            Marker::Empty,
            Marker::X,
            Marker::X,
            Marker::Empty,
            Marker::X,
            Marker::Empty,
            Marker::O,
            Marker::O,
        ];
        let result = computer.choose_move(board);
        let expected = 4;
        assert_eq!(result, expected);
    }

    #[test]
    fn negamax_blocks1() {
        let mut board: Board = Board::new(ROW_SIZE);
        let computer = Computer::new(Marker::O);
        board.spaces = vec![
            Marker::Empty,
            Marker::Empty,
            Marker::X,
            Marker::Empty,
            Marker::O,
            Marker::X,
            Marker::Empty,
            Marker::Empty,
            Marker::Empty,
        ];
        let result = computer.choose_move(board);
        let expected = 8;
        assert_eq!(result, expected);
    }

    #[test]
    fn negamax_block2() {
        let mut board: Board = Board::new(ROW_SIZE);
        let computer = Computer::new(Marker::O);
        board.spaces = vec![
            Marker::O,
            Marker::X,
            Marker::O,
            Marker::Empty,
            Marker::X,
            Marker::Empty,
            Marker::X,
            Marker::Empty,
            Marker::Empty,
        ];
        let result = computer.choose_move(board);
        let expected = 7;
        assert_eq!(result, expected);
    }

    #[test]
    fn negamax_block3() {
        let mut board: Board = Board::new(ROW_SIZE);
        let computer = Computer::new(Marker::O);
        board.spaces = vec![
            Marker::Empty,
            Marker::Empty,
            Marker::Empty,
            Marker::X,
            Marker::X,
            Marker::Empty,
            Marker::O,
            Marker::Empty,
            Marker::Empty,
        ];
        let result = computer.choose_move(board);
        let expected = 5;
        assert_eq!(result, expected);
    }

    #[test]
    fn returns_a_list_of_available_corners() {
        let mut board: Board = Board::new(ROW_SIZE);
        let computer = Computer::new(Marker::O);
        board.spaces = vec![
            Marker::X,
            Marker::O,
            Marker::Empty,
            Marker::X,
            Marker::O,
            Marker::X,
            Marker::Empty,
            Marker::X,
            Marker::Empty,
        ];
        let result = computer.get_available_corners(&board);
        let expect = vec![2, 6, 8];
        assert_eq!(result, expect);
    }
}
