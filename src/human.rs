use player::Player;
use marker::Marker;
use std::num::ParseIntError;
use board::Board;
use messages;

#[derive(Debug)]
pub struct Human {
   pub marker: Marker,
}

impl Human {
    pub fn new(marker: Marker) -> Human {
        Human {
            marker
        }
    }

    fn get_input(&self) -> String {
        use std::io;
        let mut position = String::new();
        io::stdin().read_line(&mut position).expect("Failed to read line");
        position
    }

    fn validate_input(&self, input: String) -> Result<i32, ParseIntError> {
        input.trim().parse::<i32>()
    }
}

impl Player for Human {
    fn take_turn(&self, board: &Board) -> Board {
        let board = board;
        loop {
            println!("{}", messages::take_turn_message(board.spaces.len()));
            let input = self.get_input();

            let result = self.validate_input(input);

            match result {
                Ok(val) => {
                    match board.place_marker(self.marker, val - 1) {
                        Ok(board) => {
                            break
                            board
                        },
                        Err(invalid_position_error) => {
                            println!("{}", invalid_position_error);
                            continue
                        }
                    };
                },
                Err(_) => {
                    println!("{}", messages::invalid_input());
                    continue
                },
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use marker::Marker;
    use human::Human;

    #[test]
    fn player_is_initialized_with_a_board_marker() {
        let player = Human::new(Marker::X);
        assert_eq!(player.marker, Marker::X);
    }

    #[test]
    fn validates_player_input() {
        let player = Human::new(Marker::X);
        let result = player.validate_input("3".to_string());
        assert!(result.is_ok());
        assert_eq!(result.unwrap(), 3);
    }

    #[test]
    fn errors_on_invalid_input() {
        let player = Human::new(Marker::X);
        let result = player.validate_input("asd23f".to_string());
        assert!(result.is_err());
    }
}
