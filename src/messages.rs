use board::Board;
use marker::Marker;
use rules;

pub fn display_board(board: &Board) -> String {
   let mut board_display = String::new();
    let mut space_num: i32  = 1;
    for (row_index, row) in board.spaces.chunks(board.row_size).into_iter().enumerate() {
        board_display.push_str(&create_row(row, &mut space_num));
        if row_index >= board.row_size - 1 {
            continue
        } else {
            board_display.push_str(&create_bar(board.row_size));
        }
    };
    board_display
}

fn create_row(row: &[Marker], space_num: &mut i32) -> String {
    let mut display_row = String::new();
    display_row.push_str(" ");
    for (space_in_row_index, marker) in row.into_iter().enumerate() {
        if marker == &Marker::Empty {
            display_row.push_str(&format!(" {} ", space_num));
        } else {
            display_row.push_str(&format!(" {} ", marker.to_string()));
        }
        *space_num = *space_num + 1;
        display_row.push_str(" ");
        if space_in_row_index >= row.len() - 1 {
            continue
        } else {
            display_row.push_str("| ");
        }
    };
    display_row
}

fn create_bar(row_size: usize) -> String {
    let mut bar = String::new();
    bar.push_str("\n");
    for s in 0..row_size {
        if s >= row_size - 1 {
            bar.push_str("=====");
        } else {
            bar.push_str("=====+");
        }
    }
    bar.push_str("\n");
    bar
}
pub fn take_turn_message(board_size: usize) -> String {
    format!("Please choose an empty space on the board, 1 through {}: ", board_size)
}

pub fn computer_move(marker: &Marker, position: i32) -> String {
    format!("Computer {} chose spot: {}", marker.to_string(), position)
}

pub fn end_game(board: &Board) -> String {
    let mut message = String::new();
    message.push_str("Game over!\n");
    match rules::get_winner(board) {
        Some(winner) => message.push_str(&format!("{} won!", winner.to_string())),
        None => message.push_str("Tie game!"),
    }
    message
}

pub fn invalid_input() -> String {
    String::from("Invalid input detected")
}

pub fn computer_thinking(marker: &Marker) -> String {
    String::from(format!("Computer {} is thinking...", marker.to_string()))
}

#[cfg(test)]

mod tests {

    use board::Board;
    use messages::display_board;
    use marker::Marker;
    use messages::take_turn_message;
    use messages::computer_move;
    use messages::end_game;
    use messages::invalid_input;
    use messages::computer_thinking;

    const BOARD_SIZE: usize = 9;
    const ROW_SIZE: usize = 3;
    #[test]
    fn displays_board_as_string() {
        let mut board = Board::new(ROW_SIZE);
        board.spaces = vec![Marker::X, Marker::X, Marker::Empty,
                            Marker::O, Marker::Empty, Marker::Empty,
                            Marker::Empty, Marker::O, Marker::Empty];
        let board_string = display_board(&board);
        let expected_string = "  X  |  X  |  3  \n=====+=====+=====\n  O  |  5  |  6  \n=====+=====+=====\n  7  |  O  |  9  ";
        assert_eq!(expected_string, board_string);
    }

    #[test]
    fn prompts_turn_for_user() {
        let message = take_turn_message(BOARD_SIZE);
        let expect = "Please choose an empty space on the board, 1 through 9: ";
        assert_eq!(message, expect);
    }

    #[test]
    fn prompts_user_what_position_the_computer_chose() {
        let marker = Marker::X;
        let position = 3;
        let expect = format!("Computer {} chose spot: {}", marker.to_string(), position);
        assert_eq!(computer_move(&marker, position), expect)
    }

    #[test]
    fn says_who_winner_is_at_the_end_of_the_game() {
        let mut board = Board::new(ROW_SIZE);
        board.spaces = vec![Marker::X, Marker::X, Marker::X,
                            Marker::O, Marker::Empty, Marker::Empty,
                            Marker::O, Marker::O, Marker::Empty];
        let message = end_game(&board);
        let expect = "Game over!\nX won!";
        assert_eq!(message, expect);
    }

    #[test]
    fn declares_tie_if_tie_game() {
        let mut board = Board::new(ROW_SIZE);
        board.spaces = vec![Marker::X, Marker::O, Marker::O,
                            Marker::O, Marker::X, Marker::X,
                            Marker::X, Marker::O, Marker::O];
        let message = end_game(&board);
        let expect = "Game over!\nTie game!";
        assert_eq!(message, expect);
    }

    #[test]
    fn warns_of_invalid_input() {
        let message = invalid_input();
        let expect = "Invalid input detected";
        assert_eq!(message, expect);
    }

    #[test]
    fn prompts_user_that_computer_is_working() {
        let marker = Marker::X;
        let message = computer_thinking(&marker);
        let expect = String::from("Computer X is thinking...");
        assert_eq!(message, expect)
    }
}
