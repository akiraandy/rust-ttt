extern crate rust_ttt;
extern crate clap;
use clap::App;

use clap::Arg;
use rust_ttt::modes::hvh;
use rust_ttt::modes::hvc;
use rust_ttt::modes::cvc;

fn main() {
    let matches = App::new("TTT")
        .version("0.1")
        .author("Andy Schwartz")
        .arg(Arg::with_name("HVH")
            .long("HVH")
            .value_name("row size")
            .value_name("first player symbol")
            .help("Starts a game of TTT with human players.\nEnter a number from 3 to 10 for the row size.\nEnter the letter X or O for the first player's marker.")
            .takes_value(true))
        .arg(Arg::with_name("HVC")
            .long("HVC")
            .value_name("row size")
            .value_name("first player symbol")
            .value_name("first player")
            .help("Starts a game of TTT with a human and computer player.\nEnter 3 or 4 for the row size.\nEnter the letter X or O for the first player's marker.\nEnter H if you want to go first or C if you want the computer to go first.")
            .takes_value(true))
        .arg(Arg::with_name("CVC")
            .long("CVC")
            .value_name("row size")
            .value_name("first player symbol")
            .help("Starts a game of TTT with two computer players.\nEnter 3 or 4 for the row size.\nEnter the letter X or O for the first player's marker.")
            .takes_value(true))
        .get_matches();

    if matches.is_present("HVH") {
        let mut game = hvh(matches.values_of("HVH").unwrap());
        game.play();
    } else if matches.is_present("HVC") {
        let mut game = hvc(matches.values_of("HVC").unwrap());
        game.play();
    } else if matches.is_present("CVC") {
        let mut game = cvc(matches.values_of("CVC").unwrap());
        game.play();
    }
}
