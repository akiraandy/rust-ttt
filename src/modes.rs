use board::Board;
use clap::Values;
use computer::Computer;
use game_controller::GameController;
use human::Human;
use marker::Marker;
use player::Player;

const MIN_ROW_SIZE: usize = 3;
const MAX_ROW_SIZE_COMPUTER: usize = 4;
const MAX_ROW_SIZE: usize = 10;

pub fn hvh(mut values: Values) -> GameController {
    let board = create_board(&mut values, MIN_ROW_SIZE, MAX_ROW_SIZE);
    let players = match values.next() {
        Some(symbol) => {
            if symbol == "X" || symbol == "x" {
                (
                    Box::new(Human::new(Marker::X)),
                    Box::new(Human::new(Marker::O)),
                )
            } else if symbol == "O" || symbol == "o" {
                (
                    Box::new(Human::new(Marker::O)),
                    Box::new(Human::new(Marker::X)),
                )
            } else {
                panic!("Symbol must be either X or O, got: {}", symbol);
            }
        }
        None => panic!("Symbol not supplied"),
    };
    GameController::new(vec![players.0, players.1], board)
}

pub fn hvc(mut values: Values) -> GameController {
    let board = create_board(&mut values, MIN_ROW_SIZE, MAX_ROW_SIZE_COMPUTER);
    let mut players: (Box<dyn Player>, Box<dyn Player>);
    players = match values.next() {
        Some(symbol) => {
            if symbol == "X" || symbol == "x" {
                (
                    Box::new(Human::new(Marker::X)),
                    Box::new(Computer::new(Marker::O)),
                )
            } else if symbol == "O" || symbol == "o" {
                (
                    Box::new(Human::new(Marker::O)),
                    Box::new(Computer::new(Marker::X)),
                )
            } else {
                panic!("Symbol must be either X or O, got: {}", symbol);
            }
        }
        None => panic!("Symbol not supplied"),
    };
    let order = values.next().unwrap();
    if order == "C" || order == "c" {
        players = (players.1, players.0);
    } else if order == "H" || order == "h" {
    } else {
        panic!("Player order symbol is either H or C, got: {}", order);
    };
    GameController::new(vec![players.0, players.1], board)
}

pub fn cvc(mut values: Values) -> GameController {
    let board = create_board(&mut values, MIN_ROW_SIZE, MAX_ROW_SIZE_COMPUTER);
    let players = match values.next() {
        Some(symbol) => {
            if symbol == "X" || symbol == "x" {
                (
                    Box::new(Computer::new(Marker::X)),
                    Box::new(Computer::new(Marker::O)),
                )
            } else if symbol == "O" || symbol == "o" {
                (
                    Box::new(Computer::new(Marker::O)),
                    Box::new(Computer::new(Marker::X)),
                )
            } else {
                panic!("Symbol must be either X or O, got: {}", symbol);
            }
        }
        None => panic!("Symbol not supplied"),
    };
    GameController::new(vec![players.0, players.1], board)
}

fn create_board(values: &mut Values, lower_bound: usize, upper_bound: usize) -> Board {
    match values.next() {
        Some(number) => {
            let potential_board_size = number.parse::<usize>().unwrap();
            if potential_board_size > upper_bound || potential_board_size < lower_bound {
                panic!(
                    "Row size cannot be smaller than {} or larger than {}, got: {}",
                    lower_bound, upper_bound, potential_board_size
                );
            } else {
                Board::new(potential_board_size)
            }
        }
        None => panic!("Row size not provided"),
    }
}
